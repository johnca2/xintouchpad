IFS=$'\n'
for i in $(ls -1 editable.d/main.d/conf.d) ; do

if [[ `<"editable.d/main.d/conf.d/$i/toggle.cfg"` == 1 ]] ; then
    name=$(xinput list --name-only | grep -o "$i")

    state=$(xinput --list-props "$name" | grep 'Device Enabled (' | awk -F: '{print $2}' | grep -o '[0-9]')
    if [ $state == 0 ] ; then
        xinput --enable "$name"
    else
        xinput --disable "$name"
    fi
fi
done
unset IFS
