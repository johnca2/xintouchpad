IFS=$'\n'
for i in $(ls -1 editable.d/main.d/conf.d) ; do
    if [[ `<"editable.d/main.d/conf.d/$i/useconf.cfg"` == 1 ]] ; then
        name=$(xinput list --name-only | grep "^$i$")

        code_props=$(xinput --list-props "$name" | grep 'libinput Tapping Enabled (' | grep -o '([0-9]*):' | sed 's/(\|)\|://g')
        xinput set-prop "$name" "$code_props" "$(<"editable.d/main.d/conf.d/$i/clickwhentouching.cfg")"

        code_props=$(xinput --list-props "$name" | grep 'libinput Accel Speed (' | grep -o '([0-9]*):' | sed 's/(\|)\|://g')
        xinput set-prop "$name" "$code_props" $(<"editable.d/main.d/conf.d/$i/speed.cfg")

        code_props=$(xinput --list-props "$name" | grep 'libinput Left Handed Enabled (' | grep -o '([0-9]*):' | sed 's/(\|)\|://g')
        xinput set-prop "$name" "$code_props" $(<"editable.d/main.d/conf.d/$i/reverse.cfg")

        code_props=$(xinput --list-props "$name" | grep 'libinput Scroll Method Enabled (' | grep -o '([0-9]*):' | sed 's/(\|)\|://g')
        xinput set-prop "$name" "$code_props" $(<"editable.d/main.d/conf.d/$i/displacement.cfg")

        code_props=$(xinput --list-props "$name" | grep 'libinput Natural Scrolling Enabled (' | grep -o '([0-9]*):' | sed 's/(\|)\|://g')
        xinput set-prop "$name" "$code_props" $(<"editable.d/main.d/conf.d/$i/naturaldisplacement.cfg")

        code_props=$(xinput --list-props "$name" | grep 'libinput Horizontal Scroll Enabled (' | grep -o '([0-9]*):' | sed 's/(\|)\|://g')
        xinput set-prop "$name" "$code_props" $(<"editable.d/main.d/conf.d/$i/horizontaldisplacement.cfg")

        code_props=$(xinput --list-props "$name" | grep 'libinput Disable While Typing Enabled (' | grep -o '([0-9]*):' | sed 's/(\|)\|://g')
        xinput set-prop "$name" "$code_props" $(<"editable.d/main.d/conf.d/$i/disabledwhenwriting.cfg")
    fi
done
unset IFS
