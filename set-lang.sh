while true ; do
    select=$(<editable.d/conf.d/lang-prog.cfg)
    options=$(ls -1 static.d/language.d)
    language_options=$(echo -e "$options" | grep -vs "^$select$" | sed "1i\\$select")

    io_menu2=$(yad --center --form --item-separator=$'\n' --separator=$'\n' --button="$button_back:1" --button="$button_apply:0" --field=:CB "$language_options")
    code=$?
    if [ $code == 0 ] ; then
        echo "$io_menu2" > editable.d/conf.d/lang-prog.cfg
    else
        break
    fi
    . load-lang.sh
done
