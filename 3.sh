IFS=$'\n'
for i in $(ls -1 editable.d/main.d/conf.d) ; do
    if [[ `<"editable.d/main.d/conf.d/$i/useconf.cfg"` == 1 ]] ; then
        if [[ `<"editable.d/main.d/conf.d/$i/atstart.cfg"` == 0 ]] ; then
            name=$(xinput list --name-only | grep "^$i$") #busqueda del nombre real
            xinput --disable "$name"
        fi
    fi
done
unset IFS
