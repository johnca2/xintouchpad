dep_list=(yad xinput)
IFS=$'\n'
dep_missing=($(for i in ${dep_list[@]} ; do compgen -c | grep -m1 "^$i$" 1>/dev/null 2>/dev/null || printf "$i " ; done))
unset IFS
if [[ $dep_missing ]] ; then
    echo -e "needed: ${dep_missing[*]}" && read -n1 && exit
fi
