#!/usr/bin/bash

dir0=$(dirname "$(readlink -f "$0")")
cd "$dir0"

. dat.sh
. load-lang.sh
. dep.sh

if [[ $1 == --1 ]] ; then
    . apply.sh
    . 1.sh
elif [[ $1 == --2 ]] ; then
    . 2.sh
elif [[ $1 == --3 ]] ; then
    sleep 5
    . apply.sh
    . 3.sh
elif [[ $1 == --123 ]] ; then
	f_here=$(readlink -f "$0")
	icon="${f_here%/*}/static.d/icon.d/0.svg"
	file_name="${0%.*}"
	echo -e \
"[Desktop Entry]
Exec=bash \"$f_here\" --1
Name=$entry1
Icon=$icon
Type=Application
Categories=Utility;
Terminal=false
StartupNotify=true" > "$file_name-config.desktop"

	echo -e \
"[Desktop Entry]
Exec=bash \"$f_here\" --2
Name=$entry2
Icon=$icon
Type=Application
Categories=Utility;
Terminal=false
StartupNotify=true" > "$file_name-toggle.desktop"

	echo -e \
"[Desktop Entry]
Exec=bash \"$f_here\" --3
Name=$entry3
Icon=$icon
Type=Application
Categories=Utility;
Terminal=false
StartupNotify=true" > "$file_name-atstart.desktop"
fi
