while true ; do
ls_conf=$(ls -1 editable.d/main.d/conf.d)
io_menu1=$(yad --center --always-print-result --form --item-separator=$'\n' --separator=$'\n' --button="$button_exit:1" --button="$button_language:4" --button="$button_setup:0" --button="$button_remove:3" --button="$button_add:2" --field=$text_device:CB "$ls_conf")
code=$?

if [ $code == 0 ] ; then
    while true ; do

        useconf_cfg=$(<"editable.d/main.d/conf.d/$io_menu1/useconf.cfg")
        if [[ $useconf_cfg == 0 ]] ; then
            select=$text_no
        else
            select=$text_yes
        fi
        options="$text_no\n$text_yes"
        useconf_options=$(echo -e "$options" | grep -vs "^$select$" | sed "1i\\$select")

        toggle_cfg=$(<"editable.d/main.d/conf.d/$io_menu1/toggle.cfg")
        if [[ $toggle_cfg == 0 ]] ; then
            select=$text_no
        else
            select=$text_yes
        fi
        options="$text_no\n$text_yes"
        toggle_options=$(echo -e "$options" | grep -vs "^$select$" | sed "1i\\$select")

        clickwhentouching_cfg=$(<"editable.d/main.d/conf.d/$io_menu1/clickwhentouching.cfg")
        if [[ $clickwhentouching_cfg == 0 ]] ; then
            select=$text_no
        else
            select=$text_yes
        fi
        options="$text_no\n$text_yes"
        clickwhentouching_options=$(echo -e "$options" | grep -vs "^$select$" | sed "1i\\$select")

        atstart_cfg=$(<"editable.d/main.d/conf.d/$io_menu1/atstart.cfg")
        if [[ $atstart_cfg == 0 ]] ; then
            select=$text_disable
        else
            select=$text_enable
        fi
        options="$text_enable\n$text_disable"
        atstart_options=$(echo -e "$options" | grep -vs "^$select$" | sed "1i\\$select")

        reverse_cfg=$(<"editable.d/main.d/conf.d/$io_menu1/reverse.cfg")
        if [[ $reverse_cfg == 0 ]] ; then
            select=$text_no
        else
            select=$text_yes
        fi
        options="$text_no\n$text_yes"
        reverse_options=$(echo -e "$options" | grep -vs "^$select$" | sed "1i\\$select")

        displacement_cfg=$(<"editable.d/main.d/conf.d/$io_menu1/displacement.cfg")
        if [[ $displacement_cfg == '1, 0, 0' ]] ; then
            select=$text_twofingers
        elif [[ $displacement_cfg == '0, 1, 0' ]] ; then
            select=$text_edge
        elif [[ $displacement_cfg == '0, 0, 1' ]] ; then
            select=$text_button
        fi
        options="$text_twofingers\n$text_edge\n$text_button"
        displacement_options=$(echo -e "$options" | grep -vs "^$select$" | sed "1i\\$select")

        naturaldisplacement_cfg=$(<"editable.d/main.d/conf.d/$io_menu1/naturaldisplacement.cfg")
        if [[ $naturaldisplacement_cfg == 0 ]] ; then
            select=$text_no
        else
            select=$text_yes
        fi
        options="$text_no\n$text_yes"
        naturaldisplacement_options=$(echo -e "$options" | grep -vs "^$select$" | sed "1i\\$select")

        horizontaldisplacement_cfg=$(<"editable.d/main.d/conf.d/$io_menu1/horizontaldisplacement.cfg")
        if [[ $horizontaldisplacement_cfg == 0 ]] ; then
            select=$text_no
        else
            select=$text_yes
        fi
        options="$text_no\n$text_yes"
        horizontaldisplacement_options=$(echo -e "$options" | grep -vs "^$select$" | sed "1i\\$select")

        disabledwhenwriting_cfg=$(<"editable.d/main.d/conf.d/$io_menu1/disabledwhenwriting.cfg")
        if [[ $disabledwhenwriting_cfg == 0 ]] ; then
            select=$text_no
        else
            select=$text_yes
        fi
        options="$text_no\n$text_yes"
        disabledwhenwriting_options=$(echo -e "$options" | grep -vs "^$select$" | sed "1i\\$select")

        IFS=$'\n'
        io_menu2=($(yad --center --form --title="$io_menu1" --separator=$'\n' --item-separator=$'\n' --button="$button_back:1" --button="$button_speed:2" --button="$button_apply:0" --field="$text_useconf:CB" "$useconf_options" --field="$text_toggle:CB" "$toggle_options" --field="$text_atstart:CB" "$atstart_options" --field="$text_clickwhentouching:CB" "$clickwhentouching_options" --field="$text_reverse:CB" "$reverse_options" --field="$text_naturaldisplacement:CB" "$naturaldisplacement_options" --field="$text_horizontaldisplacement:CB" "$horizontaldisplacement_options" --field="$text_displacement:CB" "$displacement_options" --field="$text_disabledwhenwriting:CB" "$disabledwhenwriting_options" --field="$button_speed:RO" "$(<"editable.d/main.d/conf.d/$io_menu1/speed.cfg")"))
        code=$?

        if [ $code == 0 ] ; then
            if [[ ${io_menu2[0]} == $text_no ]] ; then
                echo 0 > "editable.d/main.d/conf.d/$io_menu1/useconf.cfg"
            else
                echo 1 > "editable.d/main.d/conf.d/$io_menu1/useconf.cfg"
            fi

            if [[ ${io_menu2[1]} == $text_no ]] ; then
                echo 0 > "editable.d/main.d/conf.d/$io_menu1/toggle.cfg"
            else
                echo 1 > "editable.d/main.d/conf.d/$io_menu1/toggle.cfg"
            fi

            if [[ ${io_menu2[2]} == $text_disable ]] ; then
                echo 0 > "editable.d/main.d/conf.d/$io_menu1/atstart.cfg"
            else
                echo 1 > "editable.d/main.d/conf.d/$io_menu1/atstart.cfg"
            fi

            if [[ ${io_menu2[3]} == $text_no ]] ; then
                echo 0 > "editable.d/main.d/conf.d/$io_menu1/clickwhentouching.cfg"
            else
                echo 1 > "editable.d/main.d/conf.d/$io_menu1/clickwhentouching.cfg"
            fi

            if [[ ${io_menu2[4]} == $text_no ]] ; then
                echo 0 > "editable.d/main.d/conf.d/$io_menu1/reverse.cfg"
            else
                echo 1 > "editable.d/main.d/conf.d/$io_menu1/reverse.cfg"
            fi

            if [[ ${io_menu2[5]} == $text_no ]] ; then
                echo 0 > "editable.d/main.d/conf.d/$io_menu1/naturaldisplacement.cfg"
            else
                echo 1 > "editable.d/main.d/conf.d/$io_menu1/naturaldisplacement.cfg"
            fi

            if [[ ${io_menu2[6]} == $text_no ]] ; then
                echo 0 > "editable.d/main.d/conf.d/$io_menu1/horizontaldisplacement.cfg"
            else
                echo 1 > "editable.d/main.d/conf.d/$io_menu1/horizontaldisplacement.cfg"
            fi

            if [[ ${io_menu2[7]} == "$text_twofingers" ]] ; then
                echo '1, 0, 0' > "editable.d/main.d/conf.d/$io_menu1/displacement.cfg"
            elif [[ ${io_menu2[7]} == "$text_edge" ]] ; then
                echo '0, 1, 0' > "editable.d/main.d/conf.d/$io_menu1/displacement.cfg"
            elif [[ ${io_menu2[7]} == "$text_button" ]] ; then
                echo '0, 0, 1' > "editable.d/main.d/conf.d/$io_menu1/displacement.cfg"
            fi

            if [[ ${io_menu2[8]} == $text_no ]] ; then
                echo 0 > "editable.d/main.d/conf.d/$io_menu1/disabledwhenwriting.cfg"
            else
                echo 1 > "editable.d/main.d/conf.d/$io_menu1/disabledwhenwriting.cfg"
            fi
        elif [ $code == 2 ] ; then #modulo velocidad en vivo
            while true ; do
                value_speed=$(
                if [[ `<"editable.d/main.d/conf.d/$io_menu1/speed.cfg"` == 1.00 ]] ; then
                    echo '100'
                else
                    cat "editable.d/main.d/conf.d/$io_menu1/speed.cfg" | sed 's/^0.//g'
                fi)
                io_menu3=$(yad --center --scale --title="$io_menu1" --mark=50:50 --button=$button_back:1 --button=$button_apply:0 --value=$value_speed --text="$value_speed")
                code=$?
                if [[ $code == 0 ]] ; then
                    if [ $io_menu3 == 100 ] ; then
                        echo '1.00' > "editable.d/main.d/conf.d/$io_menu1/speed.cfg"
                    else
                        echo "$io_menu3" | sed 's/^/0./g' > "editable.d/main.d/conf.d/$io_menu1/speed.cfg"
                    fi
                    if [[ `<"editable.d/main.d/conf.d/$io_menu1/useconf.cfg"` == 1 ]] ; then
                        name=$(xinput list --name-only | grep "^$io_menu1$")

                        code_props=$(xinput --list-props "$name" | grep 'libinput Accel Speed (' | grep -o '([0-9]*):' | sed 's/(\|)\|://g')
                        xinput set-prop "$name" "$code_props" $(<"editable.d/main.d/conf.d/$io_menu1/speed.cfg")
                    fi
                else
                    break
                fi
            done
        else
            break
        fi

        if [[ `<"editable.d/main.d/conf.d/$io_menu1/useconf.cfg"` == 1 ]] ; then
            name=$(xinput list --name-only | grep "^$io_menu1$")

            code_props=$(xinput --list-props "$name" | grep 'libinput Tapping Enabled (' | grep -o '([0-9]*):' | sed 's/(\|)\|://g')
            xinput set-prop "$name" "$code_props" "$(<"editable.d/main.d/conf.d/$io_menu1/clickwhentouching.cfg")"

            code_props=$(xinput --list-props "$name" | grep 'libinput Accel Speed (' | grep -o '([0-9]*):' | sed 's/(\|)\|://g')
            xinput set-prop "$name" "$code_props" $(<"editable.d/main.d/conf.d/$io_menu1/speed.cfg")

            code_props=$(xinput --list-props "$name" | grep 'libinput Left Handed Enabled (' | grep -o '([0-9]*):' | sed 's/(\|)\|://g')
            xinput set-prop "$name" "$code_props" $(<"editable.d/main.d/conf.d/$io_menu1/reverse.cfg")

            code_props=$(xinput --list-props "$name" | grep 'libinput Scroll Method Enabled (' | grep -o '([0-9]*):' | sed 's/(\|)\|://g')
            xinput set-prop "$name" "$code_props" $(<"editable.d/main.d/conf.d/$io_menu1/displacement.cfg")

            code_props=$(xinput --list-props "$name" | grep 'libinput Natural Scrolling Enabled (' | grep -o '([0-9]*):' | sed 's/(\|)\|://g')
            xinput set-prop "$name" "$code_props" $(<"editable.d/main.d/conf.d/$io_menu1/naturaldisplacement.cfg")

            code_props=$(xinput --list-props "$name" | grep 'libinput Horizontal Scroll Enabled (' | grep -o '([0-9]*):' | sed 's/(\|)\|://g')
            xinput set-prop "$name" "$code_props" $(<"editable.d/main.d/conf.d/$io_menu1/horizontaldisplacement.cfg")

            code_props=$(xinput --list-props "$name" | grep 'libinput Disable While Typing Enabled (' | grep -o '([0-9]*):' | sed 's/(\|)\|://g')
            xinput set-prop "$name" "$code_props" $(<"editable.d/main.d/conf.d/$io_menu1/disabledwhenwriting.cfg")
        fi
        unset IFS
    done
elif [ $code == 2 ] ; then
    while true ; do
        ls_disp=$(xinput list --name-only | tr '[:punct:]' '.')
        io_menu2=$(yad --center --form --item-separator=$'\n' --separator=$'\n' --button="$button_back:1" --button="$button_add:0" --field=disp:CB "$ls_disp")
        code=$?
        if [ $code == 0 ] ; then
            mkdir -p "editable.d/main.d/conf.d/$io_menu2"
            cp -u static.d/default.d/conf.d/* "editable.d/main.d/conf.d/$io_menu2"
        else
            break
        fi
    done
elif [ $code == 3 ] ; then
    if [[ $io_menu1 ]] ; then
        rm -r "editable.d/main.d/conf.d/$io_menu1"
    fi
elif [ $code == 4 ] ; then
    . set-lang.sh
else
    break
fi

done
