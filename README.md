# 0.2.0

#### INTRO  
Touchpadx has been created to expand the configuration options in Desktop Managers such as: lxqt, xfce and other X11.

The version for wayland could be called `touchpadw`

Always run it the first time in the terminal, if there is an error it will most likely be reported that way.

![DEMO](demo.jpg)

#### BUG REPORT, FEATURE REQUEST, TRANSLATIONS AND DONATIONS  
email: johnca.developer@gmail.com  
reddit: https://www.reddit.com/r/touchpadx/  
paypal: johnca.developer@gmail.com 

## direct execution of the program.
setting
```
bash touchpadx.sh --1
```

toggle (keyboard shortcut)
```
bash touchpadx.sh --2
```

at start
```
bash touchpadx.sh --3
```

generate desktop entry (for integration with the system)
```
bash touchpadx.sh --123
```

## integration with the system.
**menu**  
To integrate the program with the system menu, run:
```
cp touchpadx-config.desktop ~/.local/share/applications
```

**toggle**  
To configure a keyboard shortcut and disable/enable the touchpad or other device, adapt this command to your needs:
```
bash ~/path/to/touchpadx.sh --2
```

**at start**  
To apply the settings saved at login, run:
```
cp touchpadx-config.desktop ~/.config/autostart
```

## TROUBLESHOOTING
**conflict with the system**  
If the configuration application when logging in is conflicting with the system, try increasing the file timeout in seconds: `touchpadx.sh`  
```
line 16: sleep 5
```

## DEPENDENCIES
yad  
xinput or xorg-xinput  