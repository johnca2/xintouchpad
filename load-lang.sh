touch editable.d/conf.d/lang-prog.cfg
IFS=$'\n'
lang_prog_cfg=($(<editable.d/conf.d/lang-prog.cfg))
if [[ ${#lang_prog_cfg[@]} != 1 ]] ; then
    #ls -1 static.d/language.d | sed 's/^/#/g' > editable.d/conf.d/lang-prog.cfg
    echo 'english' > editable.d/conf.d/lang-prog.cfg
    lang_prog_cfg=english
fi
unset IFS
. static.d/language.d/$lang_prog_cfg
